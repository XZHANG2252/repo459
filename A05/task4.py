#!/usr/bin/env python
# coding: utf-8

# In[5]:


#self-defined class "Bottle"
class Bottle:
    """a class modifies a basic water bottle"""
    def __init__(self, capacity, water_liter, nickname = 'N/A'):
        """This class describes bottles and the amount of water 
        contained by each bottle. The attributes include capacity and water_liter, 
        which stand for the capacity of the bottle and amount of water respectively, both in the unit of L"""
        self.capacity = capacity
        self.water_liter = water_liter
        self.nickname = nickname
    #add water to bottle    
    def add_water(amount):
        self.water_liter += amount
    #drink water
    def drink_water(amount):
        if (self.water_liter - amount)>=0: 
            self.water_liter -= amount
        else:
            self.water_liter = 0
    
    #below are 2 build_in class methods
    def __str__(self):
        return f'Bottle with nickname {self.nickname}: {self.water_liter}L/{self.capacity}L'
    
    def __gt__(self, other):
        # return True if self.capacity is greater than other,capacity
        return (self.capacity > other.capacity)
    
    
            


# In[6]:


giga = Bottle(1,0.5,'giga')
meta = Bottle(2,1,'meta')


# In[8]:


# test user defined __str__
print(giga)
print(meta)
#test user defined __gt__
print(f'giga has a larger capacity than meta: {giga > meta}')
print(f'giga has a lower capacity than meta: {giga < meta}')
#test user defined __doc__
print(giga.__doc__)


# In[ ]:




