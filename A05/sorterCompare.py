#!/usr/bin/env python
# coding: utf-8

# In[1]:


from pySort import *
import numpy as np
import sys
import argparse
import random
from time import perf_counter
import matplotlib.pyplot as plt
import copy


# In[2]:


#read in the plotBool parameter
parser = argparse.ArgumentParser()
parser.add_argument('--name', help='input 0 => plotBool = False; input 1 => plotBool = True', required=True )
args = parser.parse_args()
#print(f'Hello {args.name}')
#print(type(args.name))
if(int(args.name) == 1):
    plotBool = True
elif(int(args.name) == 0):
     plotBool = False
else:
    print('Wrong input format. Please input either 0 or 1')
    
#Cycle through different list sizes
user_sort_time = []
list_sort_time = []
np_sort_time = []

sizes = [2**10, 2**11, 2**12, 2**13, 2**14]
for n in sizes:
    #Create random lists
    A_list = []
    for a in range(0,n):
        rand_int = random.randint(-10, 10)
        A_list.append(rand_int)
    B_list = copy.deepcopy(A_list)
    A = np.array(A_list)
    
    #count user method time
    t1_start_user = perf_counter()
    sorter(A_list)
    t1_end_user = perf_counter()
    t1_user = (t1_end_user - t1_start_user)*1000.0
    user_sort_time.append(t1_user)
    
    #count list() sorting method time
    t1_start_list = perf_counter()
    B_list.sort()
    t1_end_list = perf_counter()
    t1_list = (t1_end_list - t1_start_list)*1000.0
    list_sort_time.append(t1_list)
    
    #count Numpy sorting time
    t1_start_np = perf_counter()
    A = np.sort(A)
    t1_end_np = perf_counter()
    t1_np = (t1_end_np - t1_start_np)*1000.0
    np_sort_time.append(t1_np)
    
    print(A_list[0])
    print(t1_user)
    
    print(B_list[0])
    print(t1_list)
    
    print(A[0])
    print(t1_np)
    print()

#ploting

#plotBool = True
if(plotBool):
    plt.plot(sizes,user_sort_time,label = "User defined function sorting time")
    plt.plot(sizes, list_sort_time, label = "List's sorting function time")
    plt.plot(sizes, np_sort_time, label = "Numpy sorting time")
    plt.xscale("log", base=2)
    plt.yscale("log", base = 10)
    plt.title("Time Elapsed, user defined sorter vs Python's sort vs Numpy's sort")
    plt.xlabel("Size of list/array")
    plt.ylabel("Time Elapsed in ms")
    plt.legend()
    

