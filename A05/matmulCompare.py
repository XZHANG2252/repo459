#!/usr/bin/env python
# coding: utf-8

# In[7]:


#Q1_a
import numpy as np
from pyMatmul import *
import sys
import argparse
import random
from time import perf_counter
import matplotlib.pyplot as plt


# In[11]:


#Read in the plotBool parameter 
parser = argparse.ArgumentParser()
parser.add_argument('--name', help='input 0 => plotBool = False; input 1 => plotBool = True', required=True )
args = parser.parse_args()
#print(f'Hello {args.name}')
#print(type(args.name))
if(int(args.name) == 1):
    plotBool = True
elif(int(args.name) == 0):
     plotBool = False
else:
    print('Wrong input format. Please input either 0 or 1')

#cycle through different matrix sizes
list_time = []
np_time = []
sizes = [2**5, 2**6, 2**7, 2**8]
for n in sizes:
    A_list = []
    B_list = []
    
    #Generate random  matrix
    for i in range(0,n):
        row_A = []
        row_B = []
        for j in range(0,n):
            a_num = random.uniform(-1.0, 1.0)
            b_num = random.uniform(-1.0, 1.0)
            row_A.append(a_num)
            row_B.append(b_num)
        A_list.append(row_A)
        B_list.append(row_B)
    A = np.array(A_list)
    B = np.array(B_list)
    
    #user defined method time
    t1_start_matmul = perf_counter()
    C_list = matmul(A_list, B_list)
    t1_stop_matmul = perf_counter()
    #print(f"matmul, size = {n}, time = {t1_stop_matmul-t1_start_matmul}")
    print(C_list[0][0])
    print((t1_stop_matmul-t1_start_matmul)*1000)
    list_time.append((t1_stop_matmul-t1_start_matmul)*1000)    
    
    #Numpy method
    t1_start_np = perf_counter()
    C = A@B 
    t1_stop_np = perf_counter()
    #print(f"np, size = {n}, time = {t1_stop_np-t1_start_np}")
    print(C[0][0])
    print((t1_stop_np-t1_start_np)*1000)
    np_time.append((t1_stop_np-t1_start_np)*1000)
    print()


#ploting 
if(plotBool):
    plt.plot(sizes,list_time,label = "list multiplication time")
    plt.plot(sizes, np_time, label = "Numpy multiplication time")
    plt.xscale("log", base=2)
    plt.yscale("log", base = 10)
    plt.title("Time Elapsed, user_Matmul vs Numpy_matmul")
    plt.xlabel("Size of matrix")
    plt.ylabel("Time Elapsed in ms")
    plt.legend()


# In[ ]:




