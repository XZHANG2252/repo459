#!/usr/bin/env python
# coding: utf-8

# In[1]:

#in another script timeSort.py, import the sorter function and time your function using either time.perf counter 
#or the timeit module for n = 10^3, 10^4, 10^5 and 10^6. 
#Print the first element of A before sorting, the time taken and the first element of A after sorting for each n. 
#The values inside the list A can be any random values you like. 
from time import perf_counter
from pySort import *
import random

sizes = [10**3, 10**4, 10**5,10**6]
for n in sizes:
    array_a = []
    for i in range(0,n):
        a = random.randint(1,n)
        array_a.append(a)
    
    t1_start = perf_counter()
    print("First element before sort is: ", array_a[0])
    sorter(array_a)
    t1_stop = perf_counter()
    print("Time used for the operation:",t1_stop-t1_start, ", First element of the list now is: ", array_a[0])
    print()







