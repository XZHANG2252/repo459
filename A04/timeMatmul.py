from pyMatmul import *
import random
from time import perf_counter

#In another Python script called timeMatmul.py, import the matmul function and time your
#function using either time.perf counter or the timeit module for n = 2^7, 2^8 and 2^9. 
# Print the time taken for each case. The values inside matrices A and B can be any random values you like. 

sizes = [2**7, 2**8, 2**9]
for n in sizes:
    matrix_a = []
    for i in range(0,n):
        row = []
        for j in range(0,n):
            a = random.randint(1,5)
            row.append(a);
        matrix_a.append(row)
    
    t1_start = perf_counter()
    matmul(matrix_a, matrix_a)
    t1_stop = perf_counter()
    print("Time used for the operation:",t1_stop-t1_start)

