class Student:
    # Two attributes: lastName, gpa
    # Default values as HW instruction requires
    def __init__(self, lastName = "Popescu", gpa = 3.8):
        if type(lastName) != str:
            print("Last name must be a string")
            self.lastName = "Popescu"
        else:
            self.lastName = lastName

        # if gpa is outside 0 and 4, print a line of warning    
        if gpa < 0 or gpa > 4:
            print("GPA have to be between 0 and 4")
            self.gpa = 3.8
        else:
            self.gpa = float(gpa)

    # Method: compares the GPA of two students and prints out the lastName (and nothing else) of the student with a higher GPA
    def compareGPA(self, student):
        if self.gpa > student.gpa:
            print(self.lastName)
        else:
            print(student.lastName)
