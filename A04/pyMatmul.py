#Takes two 2D lists A and B of size n × n as input arguments
#Computes the matrix product C and returns it.
def matmul (X,Y):
    result = [[sum(a*b for a,b in zip(X_row,Y_col)) for Y_col in zip(*Y)] for X_row in X]
    return result

