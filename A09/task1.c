//
// Created by cabal on 4/20/2023.
//
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "matmul.h"

double rand_range(double min, double max)
// Random double value generator
{
    double random = ((double) rand())/RAND_MAX;   // a value between 0.0 and 1.0
    double range = (max - min) * random;
    double number = min + range; // a value between min and max
    return number;
}

int main(int argc, char *argv[]) {
    //read n from command line
    char *N = argv[1];
    int n = atoi(N);

    double *A = (double*) malloc(n*n*sizeof(double));
    double *B = (double*) malloc(n*n*sizeof(double));
    double *C = (double*) malloc(n*n*sizeof(double));

    //Matrix A,B: filling in values with random double numbers in the range [-1,1]
    for(int i=0; i<n*n; i++){
        *(A+i) = rand_range(-1.0, 1.0);
    }

    for(int i=0; i<n*n; i++){
        *(B+i) = rand_range(-1.0, 1.0);
    }
    clock_t begin1 = clock();
    mmul1(A,B,C,n);
    clock_t end1 = clock();
    double time_spent_arr1 = (double)(end1 - begin1) / CLOCKS_PER_SEC;

    // Print time in ms
    printf("t\n");
    printf("%lf\n", time_spent_arr1 * 1000);
    printf("%f\n", C[n*n-1]);

    // Free memory
    free(A);
    free(B);
    free(C);

}