//
// Created by Xi Zhang on 4/20/2023.
//
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

double rand_range(double min, double max)
// Random double value generator
{
    double random = ((double) rand())/RAND_MAX;   // a value between 0.0 and 1.0
    double range = (max - min) * random;
    double number = min + range; // a value between min and max
    return number;
}

void test1(double* data, int elems, int stride){
    int i;
    double result = 0.0;
    volatile double sink;
    for (i = 0; i < elems; i += stride) {
        result += data[i];
    }/* Force compiler to not optimize away the loop */
    sink = result;
}

void test2(double* data, int elems, int stride){
    int i;
    volatile double result = 0.0;
    volatile double sink;
    for (i = 0; i < elems; i += stride) {
        result += data[i];
    }/* Force compiler to not optimize away the loop */
    sink = result;
}

int main(){
    int n_elems = 65536*8; //65536 double is 512 Kilo bytes
    double *data = (double*) malloc(n_elems*sizeof(double));
    for(int i = 0; i < n_elems; i++){
        *(data+i) = rand_range(-1.0, 1.0);
    }
    test1(data,n_elems,1);

    int stride[7] = {1, 2, 4, 8, 11, 15, 17};

    //test1

    for(int j = 0; j < 7; j++){

        double time_spent_1 = 0;
        clock_t begin1 = clock();
        for (int i = 0; i < 100; i++) {
            test1(data, n_elems, stride[j]);
        }
        clock_t end1 = clock();
        time_spent_1 = (double) (end1 - begin1) / CLOCKS_PER_SEC;
        double ave_time_1 = (time_spent_1 / 100.0)*1000.0;
        printf("%f ",ave_time_1);
    }
    printf("\n");

    //test2
    test2(data,n_elems,1);

    for(int j = 0; j < 7; j++){

        double time_spent_2 = 0;
        clock_t begin2 = clock();
        for (int i = 0; i < 100; i++) {
            test2(data, n_elems, stride[j]);
        }
        clock_t end2 = clock();
        time_spent_2 = (double) (end2 - begin2) / CLOCKS_PER_SEC;
        double ave_time_2 = (time_spent_2 / 100.0)*1000.0;
        printf("%f ",ave_time_2);
    }
    printf("\n");
    free(data);
}