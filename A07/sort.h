#ifndef SORT_H
#define SORT_H
#include <stdio.h>
// header file for bubble sort function
void sort(int* A, size_t n_elements);

#endif