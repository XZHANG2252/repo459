//
// Created by cabal on 3/14/2023.
//I used bubble sort here
#include "sort.h"
#include <stdlib.h>

void swap(int* xp, int* yp)     // exchange values pointed by 2 pointers to int
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void sort(int* A, size_t n_elements)
{
    int i, j;                                                // bubble sort, ascending order
    for (i = 0; i < n_elements - 1; i++)
        // Last i elements are already in place
        for (j = 0; j < n_elements - i - 1; j++)
            if (A[j] > A[j + 1])
                swap(&A[j], &A[j + 1]);
}