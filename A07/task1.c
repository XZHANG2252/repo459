#include <stdio.h>
#include "sort.h"
void printArray(int* int_arr, int size)     // function to print array in a line
{
    int i;
    for (i = 0; i < size; i++)
        printf("%d ", int_arr[i]);
    printf("\n");
}

int main()
{
    int arr[] = {11, 17, 20, 35, 5, 4, 9, 3, 7, 8, 1 }; // given array of int
    int n_arr = sizeof(arr) / sizeof(arr[0]); // find length of array
    printArray(arr,n_arr);  // before sorting, print
    sort(arr, n_arr);
    printArray(arr,n_arr);  // after sorting, print
    return 0;
}
