#include <stdio.h>
#include "mvmul.h"
#include <stdlib.h>
#include <time.h>

double rand_range(double min, double max)
{
    double random = ((double) rand())/RAND_MAX;   // a value between 0.0 and 1.0
    double range = (max - min) * random;
    double number = min + range; // a value between min and max
    return number;
}

int main(int argc, char *argv[]) {
    //read n from command line
    char *N = argv[1];
    int n = atoi(N);
    //int n = 3;
    //Dynamically allocate via malloc the matrix A as one long one-dimensional array having n*n double values.
    double *A = (double*) malloc(n*n*sizeof(double));
    //Dynamically allocate the vector b and c that each has n double values
    double *b = (double*) malloc(n*sizeof(double));
    double *c = (double*) malloc(n*sizeof(double ));
    //Initialize A with random numbers in the range of [-1, 1].
    for(int i=0; i<n*n; i++){
        *(A+i) = rand_range(-1.0, 1.0);
    }
   //all entries of b to be 1.0
    for(int i=0; i<n; i++){
        *(b+i) = 1.0;
    }
    //initialize all entries of c to 0.0
    for(int i=0; i<n; i++){
        *(c+i) = 0.0;
    }
    // start tracking time
    clock_t begin = clock();
/* here, do your time-consuming job */
    mvmul(A,b,c,n);
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC; //end tracking time

    printf("%lf\n", *(c+n-1));
    free(A);
    free(b);
    free(c);
    printf("%lf\n", time_spent*1000);
    return 0;

}
