#include <stdio.h>
#include <math.h>
//Bisection method for float type
float bisection_f(float start, float end){
    int N = 0;
    float mid = 0.5;
    float tol = 1E-16;

    while (N < 1000000000){
        float val = sinf(mid)/mid;
        mid = (start + end)/2;
        if (fabsf(val - 1.0) <= tol){
            return mid;
        }
       // printf("value: %f at x = %10.11e\n",val, mid);
        N += 1;
        end = mid;
    }
    return mid;
}

// Bisection method for double type
double bisection_b(double start, double end){
    int N = 0;
    double mid = 0.0;
    double tol = 1E-16;
    double val = sin(mid)/mid;
    while (N < 1000000000){
        val = sin(mid)/mid;
        mid = (start + end)/2;
        if (fabs(val - 1.0) <= tol){
            return mid;
        }

        //printf("value: %lf at x = %10.11e\n",val, mid);
        N += 1;
        end = mid;
    }
    return mid;
}

int main() {
    float a = bisection_f(0.0,1.0);
    printf("\n");
    double b = bisection_b(0.0,1.0);

    printf("float result: %10.11e\n", a);
    printf("double result: %10.11e\n", b);

    return 0;
}
