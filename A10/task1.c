//
// Created by Xi Zhang on 4/25/2023.
//
#include <limits.h>
#include <stdio.h>
int main(){
    int a = INT_MAX;
    printf("%d\n",a);
    a += 1;
    printf("%d\n",a);

    a = INT_MIN;
    printf("%d\n",a);
    a -= 1;
    printf("%d\n",a);
}