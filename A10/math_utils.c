//
// Created by XiZhang on 4/25/2023.
//

#include <stddef.h>
#include <stdlib.h>
#include <math.h>
// Returns the sum of all entries of arr.
// Uses a normal for loop, adding one value at each iteration to an accumulator.
float Sum(const float* arr, size_t count){
    float sum = 0.0;
    for(size_t i = 0; i < count; i++){
        sum += *(arr+i);
    }
    return sum;
}

// Returns the sum of the sine of each entry of arr.
// Uses a normal for loop, adding one sine value at each iteration to an
// accumulator.
float SumSines(const float* arr, size_t count){
    float sum = 0.0;
    for(size_t i = 0; i < count; i++){
        sum += sin(*(arr+i));
    }
    return sum;
}

// Returns the sum of entries of arr in the value pointed to by sum, and
// returns the sum of the sines of the entries of arr the value pointed to by sum_sines.
// There should only be a single for loop in this function, which calculates
// both quantities in the body of the for loop.
void Fusion(const float* arr, size_t count, float* sum, float* sum_sines){
    for(size_t i = 0; i < count; i++){
        *sum += *(arr+i);
        *sum_sines += sin(*(arr+i));
    }
}
