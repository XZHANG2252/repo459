How to run:
	ns_solver.py       Directly runnable
	ns_solver.ipynb    Directly runnable
	ns_solver.mlx	   Directly runnable

The following variables can be changed in order to generate resutls for different boundary conditions, time steps and convergence criteria

Variables that define the mesh grid
	Lx: Length along x-dir
	Ly: Length along y-dir
	dx: spatial step size along x-dir
	dy: spatial step size along y-dir

Variables that define boundary conditions
	nu: kinematic viscosity
	U_lid: initial velocity at top boundary
	roh: density of fluid
	Tol_fac: tolerance for determing convergence

Variables for transient calculation
	dt: time step
	t_final: final time 
	n_iterate: number of iteration for time step 