//
// Created by xiz on 4/10/2023.
//

#include "matmul.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

double rand_range(double min, double max)
{
    double random = ((double) rand())/RAND_MAX;   // a value between 0.0 and 1.0
    double range = (max - min) * random;
    double number = min + range; // a value between min and max
    return number;
}

int main() {
    int n = 1024;
    //Dynamically allocate via malloc the matrix A as one long one-dimensional array having n*n double values.
    double *A = (double*) malloc(n*n*sizeof(double));
    double *B = (double*) malloc(n*n*sizeof(double));
    double *C = (double*) malloc(n*n*sizeof(double));

    //Initialize A with random numbers in the range of [-1, 1].
    for(int i=0; i<n*n; i++){
        *(A+i) = rand_range(-1.0, 1.0);
    }

    for(int i=0; i<n*n; i++){
        *(B+i) = rand_range(-1.0, 1.0);
    }

// calculate the time needed
    clock_t begin1 = clock();
    mmul1(A, B, C, n);
    clock_t end1 = clock();
    double time_spent_arr1 = (double)(end1 - begin1) / CLOCKS_PER_SEC;

    clock_t begin2 = clock();
    mmul2(A, B, C, n);
    clock_t end2 = clock();
    double time_spent_arr2 = (double)(end2 - begin2) / CLOCKS_PER_SEC;

    clock_t begin3 = clock();
    mmul3(A, B, C, n);
    clock_t end3 = clock();
    double time_spent_arr3 = (double)(end3 - begin3) / CLOCKS_PER_SEC;

    clock_t begin4 = clock();
    mmul4(A, B, C, n);
    clock_t end4 = clock();
    double time_spent_arr4 = (double)(end4 - begin4) / CLOCKS_PER_SEC;

    // Print time for methods #1 to 4
    printf("t1\n");
    printf("%lf\n", time_spent_arr1 * 1000);
    printf("t2\n");
    printf("%lf\n", time_spent_arr2 * 1000);
    printf("t3\n");
    printf("%lf\n", time_spent_arr3 * 1000);
    printf("t4\n");
    printf("%lf\n", time_spent_arr4 * 1000);
    free(A);
    free(B);
    free(C);
    return 0;

}
