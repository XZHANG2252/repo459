//
// Created by xiz on 4/10/2023.
//
#include "sumArray.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

double rand_range(double min, double max)
{
    double random = ((double) rand())/RAND_MAX;   // a value between 0.0 and 1.0
    double range = (max - min) * random;
    double number = min + range; // a value between min and max
    return number;
}


int main(int argc, char *argv[]) {
    //read n from command line
    char *N = argv[1];
    int n = atoi(N);
    //int n = 3;
    //Dynamically allocate via malloc the matrix A as one long one-dimensional array having n*n double values.
    double *A = (double*) malloc(n*n*sizeof(double));
    //Initialize A with random numbers in the range of [-1, 1].
    for(int i=0; i<n*n; i++){
        *(A+i) = rand_range(-1.0, 1.0);
    }

    // start tracking time
    clock_t begin1 = clock();
/* here, do your time-consuming job */
    double sum_1 = 0.0;
    sum_1 = sumArray1(A,n);
    clock_t end1 = clock();
    double time_spent_arr1 = (double)(end1 - begin1) / CLOCKS_PER_SEC; //end tracking time

    clock_t begin2 = clock();
    double sum_2 = 0.0;
    sum_2 = sumArray2(A,n);
    clock_t end2 = clock();
    double time_spent_arr2 = (double)(end2 - begin2) / CLOCKS_PER_SEC; //end tracking time


    printf("t1\n");
    printf("%lf\n", time_spent_arr1 * 1000);

    printf("t2\n");
    printf("%lf\n", time_spent_arr2 * 1000);
    free(A);
    return 0;

}

