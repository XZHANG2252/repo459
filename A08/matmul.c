//
// Created by cabal on 4/10/2023.
//
#include<stdio.h>
void mmul1(const double* A, const double* B, double *C, const size_t n){
    for(size_t i = 0; i < n; i++){
        for(size_t j = 0; j < n; j++){
            for(size_t k = 0; k < n; k++){
                C[i*n+j] += A[i*n+k] * B[k*n+j];
            }
        }
    }
}
void mmul2(const double* A, const double* B, double *C, const size_t n){
    for(size_t i = 0; i < n; i++){
        for(size_t j = 0; j < n; j++){
            for(size_t k = 0; k < n; k++){
                C[j*n+i] += A[j*n+k] * B[k*n+i];
            }
        }
    }

}
void mmul3(const double* A, const double* B, double *C, const size_t n){
    for(size_t i = 0; i < n; i++){
        for(size_t j = 0; j < n; j++){
            for(size_t k = 0; k < n; k++){
                C[i*n+j] += A[i*n+k] * B[j*n+k];
            }
        }
    }

}
void mmul4(const double* A, const double* B, double *C, const size_t n){
    for(size_t i = 0; i < n; i++){
        for(size_t j = 0; j < n; j++){
            for(size_t k = 0; k < n; k++){
                C[i*n+j] += A[k*n+i] * B[k*n+j];
            }
        }
    }

}
