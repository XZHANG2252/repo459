#include <stdio.h>
#include <malloc.h>
#include "structs.h"

int main() {
    //Prints out the size (in bytes) of each structure, A and B each on its own line.

    printf("size of structA: %d\n", sizeof(struct A));
    printf("size of structB: %d\n", sizeof(struct B));
    struct A *myPointerA;
    myPointerA = (struct A *)malloc(sizeof(struct A));
//    struct B *myPointerB;
//    myPointerB = (struct B *)malloc(sizeof(struct B));



    //Sets each field of the struct to whatever value you like
    myPointerA->i = 89;
    myPointerA->c = '@';
    myPointerA->d = 7.62;

//    myPointerB->i = 59;
//    myPointerB->d = 100.0;
//    myPointerB->c = 'g';
    //Prints each field of the struct on its own line in the order that they appear in the declaration of struct A
    printf("structA->i = %d\n", myPointerA->i);
    printf("structA->c = %c\n", myPointerA->c);
    printf("structA->d = %lf\n", myPointerA->d);
    //Frees the structs
    //printf("%d",sizeof(myPointerA));
    free(myPointerA);
//    free(myPointerB);

    return 0;
}
