#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// This function defines the comparison method for descending order
int int_sorter(const void *first_arg, const void *second_arg) {
    // cast the pointers to the right type
    int frst = *(int *)first_arg;
    int scnd = *(int *)second_arg;
    // carry out the comparison
    if (frst < scnd) {
        return 1;
    } else if (frst == scnd) {
        return 0;
    } else {
        return -1;
    }
}


int main(int argc, char *argv[]) {
    //check if there's extra argument
    if (argc != 2){
        return 0;
    }
    char *N = argv[1];
    //check if N contains any non-digit numbers (eg. '-', '.')
    for (int i = 0; i < strlen(N); i++){
        if(!isdigit(N[i])){
            return 0;
        }
    }
    //convert N to int
    int N_int = atoi(N);
    if (N_int <= 0){
        return 0;
    }
    //make array
   int *arr_int = (int *) malloc((N_int+1)*sizeof(int));
    for (int i=0; i<N_int+1;i++){
        arr_int[i] = i;
    }
    //print array in ascending order
    for (int i=0; i<N_int+1;i++) {
        printf("%d ", arr_int[i]);
    }
    printf("\n");
    qsort(arr_int, (N_int+1), sizeof(int), int_sorter);

    //print array in descending order
    for (int i=0; i<N_int+1;i++) {
        printf("%d ", arr_int[i]);
    }
    printf("\n");

    //free array
    free(arr_int);
    return 0;
}
